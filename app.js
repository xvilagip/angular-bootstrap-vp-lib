/**
 * Created by petervilagi on 28/01/16.
 */

angular.module
(
	"vp_lib.app",
	[
		"frapontillo.bootstrap-switch",
		"ui.bootstrap",
		"vp_lib.flash",
		"vp_lib.html",
		"vp_lib.localisation",
		"jcs-autoValidate"
	]
);

angular.module("vp_lib.app")
	.run([
		"$rootScope",
		"$location",
		"$anchorScroll",
		"dateFormat",
		"dateFormatWithoutYear",
		"timeFormat",
		"timeFormatLong",
		"bootstrap3ElementModifier",
		"validator",
		"localeService",
		"defaultErrorMessageResolver",
		"spaceChar",
		function ($rootScope, $location, $anchorScroll, dateFormat, dateFormatWithoutYear, timeFormat, timeFormatLong, bootstrap3ElementModifier, validator, localeService, defaultErrorMessageResolver, spaceChar)
		{
			//	validator icons
			bootstrap3ElementModifier.enableValidationStateIcons(true);

			validator.setValidElementStyling(true);
			validator.setInvalidElementStyling(true);

			defaultErrorMessageResolver.setCulture('sk-SK');

			//	DATE formats
			$rootScope.dateFormat				= dateFormat;
			$rootScope.dateFormatWithoutYear	= dateFormatWithoutYear;
			$rootScope.dateTimeFormat			= dateFormat + ' ' + timeFormat;
			$rootScope.dateTimeFormatLong		= dateFormat + ' ' + timeFormatLong;

			//	FN definitions
			$rootScope.keys = Object.keys;

			$rootScope.objectLength = function(obj, defaultCount)
			{
				var length = defaultCount !== undefined ? defaultCount : 0;

				if ( obj !== undefined && obj !== null )
				{
					if ( angular.isObject(obj) )
					{
						length = Object.keys(obj).length;
					}
					else if ( angular.isArray(obj) )
					{
						length = obj.length;
					}
				}

				return length;
			};

			$rootScope.convertToInt = function(id)
			{
				return parseInt(id, 10);
			};
			$rootScope.convertToBool = function(val)
			{
				return ( val == "true" );
			};
			$rootScope.spaceChar = spaceChar;
			$rootScope.isEmpty = function(obj)
			{
				for(var prop in obj)
				{
					if(obj.hasOwnProperty(prop))
					{
						return false;
					}
				}
				return true;
			};

			$rootScope.equals = function(obj1, obj2)
			{
				return angular.equals(obj1, obj2);
			};

			$rootScope.scrollTo = function(id)
			{
				var old = $location.hash();
				$location.hash(id);
				$anchorScroll();
				//reset to old to keep any additional routing logic from kicking in
				$location.hash(old);
			};

			localeService.setLocale('sk');

			$rootScope.stringify = function(arrayValue, delimitter)
			{
				return angular.isArray(arrayValue) ? arrayValue.join(delimitter) : arrayValue;
			};
		}
	])

	.value("spaceChar", "&nbsp;")

	.constant('LIB_NAME', "angular-bootstrap-vp-lib")

	.filter('number', [function()
	{
		return function(input)
		{
			return parseInt(input, 10);
		};
	}])


	.constant('VALIDATION_PATTERNS',
		{
			"msisdn": /42[0-1][1-9][0-9]{8}$|420[67][0-9]{8}$/
		}
	)

	.filter('explodeTextToMultiLine', function()
	{
		return function(text)
		{
			return ( text === undefined || angular.equals(null, text) || angular.equals("", text) ) ? "" : text.replace(/\n/g, '\<br\/\>');
		}
	})

	.filter('unbreakable', function(spaceChar)
	{
		return function(text)
		{
			return text.replace(/\ /g, spaceChar);
		}
	})

	.filter('number', [function()
	{
		return function(input)
		{
			return parseInt(input, 10);
		};
	}])

	.filter('money', ["currency", function(currency)
	{
		//	2 decimal places
		return function(input)
		{
			var a = input * 100;
			a = Math.round(a);
			a = a / 100;

			return a + "&nbsp;" + currency;
		};
	}])

	.filter('zip', function ()
	{
		return function (zip)
		{
			if ( ! zip )
			{
				return '';
			}

			var value = zip.toString().trim().replace(/[ \+\-]/g, '');

			switch ( value.length )
			{
				//	shorter dealing code
				case 5:
					zip = value.slice(0, 3) + ' ' + value.slice(3);
					break;
			}

			return zip;
		};
	})

	.filter('msisdn', function ()
	{
		return function (tel)
		{
			if ( ! tel )
			{
				return '';
			}

			var value = tel.toString().trim().replace(/[ \+]/g, '');

			if ( value.match(/[^0-9]/) )
			{
				return tel;
			}

			var country,
				mno,
				number
			;

			switch (value.length)
			{
				//	shorter dealing code
				case 11:
					country	= value.slice(0, 2);
					mno		= value.slice(2, 5);
					number	= value.slice(5);
					break;

				//	classic format with dealing code
				case 12:
					country	= value.slice(0, 3);
					mno		= value.slice(3, 6);
					number	= value.slice(6);
					break;

				default:
					return tel;
			}
			//console.log(value.length, country, mno, number);

			number = number.slice(0, 3) + " " + number.slice(3);

			return '+' + (country + " (0)" + mno + " " + number).trim();
		};
	})

	.value('dateFormat', "dd.MM.yyyy")
	.value('dateFormatWithoutYear', "dd.MM.")
	.value('timeFormatLong', "HH:mm:ss")
	.value('timeFormat', "HH:mm")
;


