/**
 * Created by petervilagi on 28/01/16.
 */

angular.module(
	"vp_lib.html",
	[]
)
	.service('htmlService', function ($window)
	{
		var object = {};

		object.openLinkInNewWindow = function(url)
		{
			$window.open(url);
		};

		object.redirect = function(url)
		{
			$window.location.href = url;
		};

		return object;
	})

;
