/**
 * Created by petervilagi on 28/01/16.
 */

angular.module(
	"vp_lib.flash",
	[]
)

	.constant("fatalErrorFlashDetails", {
		"text": "Ooppsss... Pri vykonaní požadovanej akcie sa stala chyba. Prosím kontaktujte nás.",
		"icon":	"fa-lg fa-frown-o",
		"mood":	"danger"
	})


	.run(['$anchorScroll', function($anchorScroll)
	{
		//	always scroll by 70 extra pixels because of the static navbar
		$anchorScroll.yOffset = 70;
	}])


	.factory("flashMessageFactory", function($rootScope, fatalErrorFlashDetails)
	{
		var flashMessages		= null,
			currentMessages		= {},
			methods				= {},
			reset				= function()
			{
				flashMessages		= null;
			},
			manageMessages		= function()
			{
				if ( ! angular.isUndefined(flashMessages) && angular.isObject(flashMessages) )
				{
					currentMessages	= flashMessages;
					reset();
				}
				else if ( ! angular.equals({}, currentMessages) )
				{
					currentMessages 	= {};
				}
			}
		;

		//	------------------------------------------------------------------------------------------------------------

		/**
		 * Call this after page redirection
		 */
		$rootScope.$on('$routeChangeSuccess', function(event, next)
		{
			//	handle double redirect (removing '/' from the end of the new path)
			if ( ! angular.isUndefined(next) && ! angular.isUndefined(next.$$route) && ( angular.isUndefined(next.$$route.redirectTo) || angular.equals(null, next.$$route.redirectTo) ) )
			{
				manageMessages();
			}
		});

		//	------------------------------------------------------------------------------------------------------------

		/**
		 * Delete old messages
		 */
		methods.reset = function ()
		{
			reset();
		};

		methods.rrr = function ()
		{
			currentMessages 	= {};
			reset();
		};

		/**
		 * Add message to display it without redirection
		 *
		 * @param text
		 * @param mood
		 * @param icon
		 */
		methods.addAndDisplay = function(text, mood, icon)
		{
			methods.add(text, mood, icon);

			manageMessages();
		};

		/**
		 * Add message about a fatal error (E500)
		 */
		methods.reportFatalError = function()
		{
			methods.addAndDisplay(fatalErrorFlashDetails.text, fatalErrorFlashDetails.mood, fatalErrorFlashDetails.icon);
		};

		/**
		 * Add new flash message
		 *
		 * @param text
		 * @param mood
		 * @param icon
		 */
		methods.add = function(text, mood, icon)
		{
			//console.log(text, mood, icon);

			if ( text !== undefined )
			{
				var addMessage = true;

				if ( angular.isUndefined(flashMessages) || ! angular.isObject(flashMessages) )
				{
					flashMessages = {};
				}

				if ( angular.equals({}, flashMessages) || angular.isUndefined(flashMessages[mood]) )
				{
					flashMessages[mood] = [];
				}
				else
				{
					angular.forEach(flashMessages[mood], function(value, key)
					{
						if ( value.text === text )
						{
							addMessage = false;
						}
					});
				}

				if ( addMessage )
				{
					flashMessages[mood].push(
						{
							"mood": mood,
							"text": text,
							"icon":	icon
						}
					);
				}
			}

		};

		/**
		 * Get flash messages
		 *
		 * @returns {{}}
		 */
		methods.get = function()
		{
			return currentMessages;
		};

		//	------------------------------------------------------------------------------------------------------------

		return methods;
	})


	.directive("flashMessage", function()
	{
		return {
			restrict:	"E",
			transclude:	true,
			scope:
			{
				"icon":		"@",
				"type":		"@",
				"close":	"=",
				"index":	"="
			},
			template:	'<div ng-if="! close" uib-alert class="alert alert-{{type || \'warning\'}}" role="alert"><i ng-if="obj.icon" class="fa {{icon}} flash_icon"></i> <span ng-transclude></span></div>' +
						 '<div ng-if="close" uib-alert class="alert alert-{{type || \'warning\'}}" close="close(index, type)" role="alert"><i ng-if="obj.icon" class="fa {{icon}} flash_icon"></i> <span ng-transclude></span></div>'
		}
	})


	.directive("fatalFlashMessage", function(fatalErrorFlashDetails)
	{
		return {
			restrict:	"E",
			scope:
			{
				"text":	"@",
				"icon":	"@",
				"mood":	"@"
			},
			link:		function(scope, element, attrs)
			{
				scope.defaults = fatalErrorFlashDetails;
			},
			template:	'<flash-message type="{{mood || defaults.mood}}" icon="{{icon || defaults.icon}}">{{text || defaults.text}}</flash-message>'
		}
	})


	.directive("flashMessageList", function(flashMessageFactory, $anchorScroll, $location)
	{
		return {
			restrict:   "E",
			scope:      true,
			template:	'<div id="flash_message_list" ng-if="flashes" ng-repeat="(mood, msgByMood) in flashes">' +
							'<flash-message ng-repeat="obj in msgByMood track by obj.text" ng-attr-type="{{mood}}" ng-attr-icon="{{obj.icon}}" ng-attr-close="closeFlashMessage" ng-attr-index="index">' +
						 		'<i ng-if="obj.icon" class="fa {{obj.icon}} flash_icon"></i> {{obj.text}}' +
						 	'</flash-message>' +
						 '</div>',

			link:		function(scope, element, attrs)
						{
							var gotoFlashMessagesContainer = function()
							{
								var newHash = 'flash_message_list';

								//	$anchorScroll will automatically scroll to it
								$anchorScroll();
							};

							scope.flashes = flashMessageFactory.get();

							scope.$watch(
								function()
								{
									return flashMessageFactory.get();
								},
								function(newValue)
								{
									if ( ! angular.isUndefined(newValue) && angular.isObject(newValue) && ! angular.equals({}, newValue) )
									{
										scope.flashes = newValue;

										gotoFlashMessagesContainer();

										//	reset the flash message array after displaying it!!!! ;)
										flashMessageFactory.reset();
									}
								}
							);

							scope.closeFlashMessage = function(index, mood)
							{
								scope.flashes[mood].splice(index, 1);
							};

						}
		}
	})
;
