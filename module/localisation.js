/**
 * Created by petervilagi on 28/01/16.
 */

angular.module(
	"vp_lib.localisation",
	[]
)

	.service("localeService", function()
	{
		var locale = "";

		var methods = {

		};

		methods.setLocale = function(localeInput)
		{
			moment.locale(localeInput);
		};

		methods.test = function()
		{
			console.log(moment().format('L'));
		};


		methods.convertToDbFormat = function(dateObject, useDate)
		{
			var output = null;

			var format = ( useDate === true ) ? "YYYY-MM-DD" : "YYYY-MM-DD HH:mm:ss";

			if ( dateObject && ! angular.isUndefined(dateObject) )
			{
				if ( ! angular.isDate(dateObject) )
				{
					dateObject = new Date(dateObject);
				}

				output = moment(dateObject).format(format);
			}

			return output;
		};

		return methods;
	})
;
