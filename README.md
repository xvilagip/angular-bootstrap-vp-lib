# README #

angular-bootstrap-vp-lib

* Basic directives and other stuff to make life easier while working with angular, bootstrap
* v1.0
* [peter vilagi - petrvilagi@gmail.com](https://bitbucket.org/xvilagip/angular-bootstrap-vp-lib)

### How do I get set up? ###

#	Dependencies:
	script(src='/resources/angular-bootstrap-vp-lib/app.js')
    
	script(src='/resources/angular-bootstrap-vp-lib/module/flash.js')
	script(src='/resources/angular-bootstrap-vp-lib/module/html.js')
	script(src='/resources/angular-bootstrap-vp-lib/module/localisation.js')

	script(src='/resources/angular-bootstrap-vp-lib/resource/angular-bootstrap-switch/angular-bootstrap-switch.min.js')
	script(src='/resources/angular-bootstrap-vp-lib/resource/bootstrap-switch-master/js/bootstrap-switch.min.js')
	link(rel='stylesheet', href='/resources/angular-bootstrap-vp-lib/resource/bootstrap-switch-master/css/bootstrap-switch.min.css')
	link(rel='stylesheet', href='/resources/angular-bootstrap-vp-lib/resource/font-awesome/css/font-awesome.min.css')
	script(src='/resources/angular-bootstrap-vp-lib/resource/angular-bootstrap/ui-bootstrap.min.js')
	script(src='/resources/angular-bootstrap-vp-lib/resource/angular-auto-validate/jcs-auto-validate.min.js')
	
	script(src='/resources/angular-bootstrap-vp-lib/resource/modernizr/modernizr-inputtypes.min.js')
	
	script(src='/resources/angular-bootstrap-vp-lib/resource/moment/moment-with-locales.min.js')
	script(src='/resources/angular-bootstrap-vp-lib/resource/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')
	link(rel='stylesheet', href='/resources/angular-bootstrap-vp-lib/resource/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')
	
	script(src='/resources/angular-bootstrap-vp-lib/directive/validatorsDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/buttonDirectives.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/filtersDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/formDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/checkboxDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/dateTimePickerDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/dataWrapperDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/addressDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/contactListDirective.js')
	script(src='/resources/angular-bootstrap-vp-lib/directive/gpsDirective.js')
	
	link(rel='stylesheet', href='/resources/angular-bootstrap-vp-lib/css/vp_lib.css')

#	minified version for directives
	script(src='/resources/angular-bootstrap-vp-lib/directives.min.js')
	

